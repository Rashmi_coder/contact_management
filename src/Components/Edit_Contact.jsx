import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { editContact } from '../Redux/action';


function EditContact() {
    
    const { id } = useParams()
    console.log(id)

    const dispatch = useDispatch()

    const AllContact = useSelector((store) => store.contacts)

    const [form, setForm] = useState({})

    const handleChange = (e) => {
     
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    function handleSave() {
        dispatch(editContact({ id, ...form }))
    }

    useEffect(() => {

        AllContact.filter((el) => el.id == id && setForm(el))

    }, [])
    return (
        <div className="w-1/2 mx-auto my-4 pt-16 bg-slate-200 shadow-md rounded px-8 pt-6 pb-8 mb-4 mt-16">
            <h2 className="text-2xl font-bold mb-4 text-black">Edit Contact</h2>
            <div className="mb-4">
                <input
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="first-name"
                    type="text"
                    name="first_name"
                    value={form.first_name}
                    onChange={handleChange}
                    placeholder='First Name'
                />
            </div>
            <div className="mb-4">
                <input
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="last-name"
                    type="text"
                    name="last_name"
                    value={form.last_name}
                    onChange={handleChange}
                    placeholder='Last Name'
                />
            </div>
            <div className="mb-4">
                
                <input
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="last-name"
                    type="number"
                    name="mob"
                    value={form.mob}
                    onChange={handleChange}
                    placeholder='Mobile Number'
                />
            </div>
            <div className="mb-4">
                
                <select
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="status"
                    name="status"
                    value={form.status}
                    onChange={handleChange}
                >
                    <option disabled>Choose Status</option>
                    <option value={'active'}>Active</option>
                    <option value={"inactive"}>Inactive</option>
                </select>
            </div>
            <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                onClick={handleSave}
            >
                Update Contact
            </button>
        </div>
    );
}


export default EditContact