import { Link } from "react-router-dom"
import { FcContacts } from "react-icons/fc";
import { TbChartHistogram } from "react-icons/tb";
export default function Sidebar() {
   
    return (
        <div className="flex border-r-2">
            <div className="flex pt-16 flex-col h-screen p-3 shadow w-60 bg-slate-300">
                <div className="space-y-3">
                    <div className="flex items-center">
                        <h3 className="text-xl mt-4 ml-3 font-bold">Dashboard</h3>
                    </div>
                    <div className="flex-1">
                        <ul className="pt-2 pb-4 space-y-1 text-sm">
                            <li className="rounded-sm">
                                <Link to="/" className="flex items-center p-2 space-x-3 rounded-md">
                                    <FcContacts className='text-xl'/>
                                    <span>Contacts</span>
                                </Link>
                            </li>
                            <li className="rounded-sm">
                                <Link to="/dashboard" className="flex items-center p-2 space-x-3 rounded-md">
                                   <TbChartHistogram className="text-xl text-orange-700"/>
                                    <span>Charts And Maps</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    );
}