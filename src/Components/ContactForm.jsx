import { useState } from 'react';
import { useDispatch } from "react-redux";
import { addContact } from '../Redux/action';
function ContactForm() {


    const dispatch = useDispatch()

    const [form, setForm] = useState({
        first_name: "",
        last_name: "",
        mob: "",
        status: "active"
    })

    const handleChange = (e) => {


        setForm({
            ...form,
            [e.target.name]: e.target.value
        })


    }




    function handleSave() {



        dispatch(addContact(form))

    }

    return (
        <div className="w-1/2 mx-auto my-4 pt-16 bg-slate-200 shadow-md rounded px-8 pt-6 pb-8 mb-4 mt-16">
            <h2 className="text-2xl font-bold mb-4 text-black">Create Contact</h2>
            <div className="mb-4">
                <input
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="first-name"
                    type="text"
                    name="first_name"
                    value={form.first_name}
                    onChange={handleChange}
                    placeholder='First Name'
                />
            </div>
            <div className="mb-4">
                <input
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="last-name"
                    type="text"
                    name="last_name"
                    value={form.last_name}
                    onChange={handleChange}
                    placeholder='Last Name'
                />
            </div>
            <div className="mb-4">
                
                <input
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="last-name"
                    type="number"
                    name="mob"
                    min='10'
                    max='10'
                    value={form.mob}
                    onChange={handleChange}
                    placeholder='Mobile Number'
                />
            </div>
            <div className="mb-4">
                
                <select
                    className="w-full border border-gray-400 p-2 rounded-md"
                    id="status"
                    name="status"
                    value={form.status}
                    onChange={handleChange}
                >
                    <option disabled>Choose Status</option>
                    <option value={'active'}>Active</option>
                    <option value={"inactive"}>Inactive</option>
                </select>
            </div>
            <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                onClick={handleSave}
            >
                Save Contact
            </button>
        </div>

//         <div className="w-full max-w-xs mt-10">
//   <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
//     <div className="mb-4">
//       <label className="block text-gray-700 text-sm font-bold mb-2" for="username">
//         Username
//       </label>
//       <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username"/>
//     </div>
//     <div className="mb-6">
//       <label className="block text-gray-700 text-sm font-bold mb-2" for="password">
//         Password
//       </label>
//       <input className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="******************"/>
//       <p className="text-red-500 text-xs italic">Please choose a password.</p>
//     </div>
//     <div className="flex items-center justify-between">
//       <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
//         Sign In
//       </button>
//       <a className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="#">
//         Forgot Password?
//       </a>
//     </div>
//   </form>
//   <p className="text-center text-gray-500 text-xs">
//     &copy;2020 Acme Corp. All rights reserved.
//   </p>
// </div>
    );
}


export default ContactForm