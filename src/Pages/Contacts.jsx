
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import Popup from "../Components/Popup"
import { removeContact } from "../Redux/action"
import { IoMdAddCircle } from "react-icons/io";
import { FaEdit, FaUser } from "react-icons/fa";
import { MdDelete } from 'react-icons/md';
const Contacts = () => {
    const AllContacts = useSelector((store) => store.contacts)
    const dispatch = useDispatch()
    console.log(AllContacts)

    useEffect(() => {

    }, [dispatch, AllContacts.length])
    return (
        <div className="justify-center pt-16 text-gray-50   p-4  w-full ">
            <div className="m-4 flex justify-between my-2">
                <button className="rounded-full flex bg-violet-500 items-center space-x-1.5">
                    <Link to="/contact_form" className="flex items-center p-2 space-x-3 rounded-md">
                        <IoMdAddCircle className="text-lg" />
                        Add Contact
                    </Link>
                </button>

            </div>


            {AllContacts.length === 0 && <div className=" m-auto w-fit p-4 align-middle text-blue-500 justify-center">
                <div>
                    <div className="flex flex-col md:flex-row py-4 md:py-0 md:h-[80vh] justify-center items-center bg-white space-x-8">
                        <div>
                            <img src="/page-notfound.png" alt="page not found" width="200" height="300" />
                        </div>
                        <div className="text-black">
                            <p className="text-lg md:text-4xl font-semibold my-2">Someone Ate The Page.</p>
                            <p>No Contact Found Please add contact from</p>
                            <p>Add Contact Button.</p>
                        </div>
                    </div>
                </div>

            </div>}
            <div id="contact_list" className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-4">

                {
                    AllContacts.map((el) => {
                        console.log("aaaaaaaa",el)
                        return <div key={el.id} className="bg-slate-300 rounded-lg shadow-md m-4 p-4 text-white">
                            <div className="flex justify-between my-2">
                                <Link to={`edit/${el.id}`}>
                                    <FaEdit className='text-lg text-sky-700' />
                                </Link>
                                <MdDelete className="text-2xl text-red-600 hover:text-red-700 cursor-pointer" onClick={() => dispatch(removeContact(el.id))} />
                            </div>
                            <div className="w-3/4 m-auto">
                                <FaUser className="w-full rounded-full text-5xl mb-4 text-blue-700" />
                                <div className="text-left text-black">
                                    <p>First Name : {el.first_name}</p>
                                    <p>Last Name  : {el.last_name}</p>
                                    <p>Mobile     : {el.mob}</p>
                                    <p>Status     : {el.status === "active" ? "Active" : "Inactive"}</p>
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default Contacts